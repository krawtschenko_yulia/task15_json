package me.krawtschenko.yulia;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonRootName("gems")
@JsonTypeName("gem")
public class Gem {
    private String name;
    private String preciousness;
    private String origin;
    private VisualParameters visualParameters;
    private float caratWeight;
    private String certification;
    private float value;

    public Gem() { }

    public Gem(String name, String preciousness, String origin,
               String colour, boolean transparency, String clarity, String cut,
               boolean colourChange, boolean catsEye,
               float caratWeight, String certification, float value) {

        this.name = name;
        this.preciousness = preciousness;
        this.origin = origin;
        this.visualParameters = new VisualParameters(colour, transparency,
                clarity, cut, colourChange, catsEye);
        this.caratWeight = caratWeight;
        this.certification = certification;
        this.value = value;
    }

    public Gem(String name, String preciousness, String origin,
               VisualParameters visualParameters,
               float caratWeight, String certification, float value) {

        this.name = name;
        this.preciousness = preciousness;
        this.origin = origin;
        this.visualParameters = visualParameters;
        this.caratWeight = caratWeight;
        this.certification = certification;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPreciousness() {
        return preciousness;
    }

    public void setPreciousness(String preciousness) {
        this.preciousness = preciousness;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public float getCaratWeight() {
        return caratWeight;
    }

    public void setCaratWeight(float caratWeight) {
        this.caratWeight = caratWeight;
    }

    public String getCertification() {
        return certification;
    }

    public void setCertification(String certification) {
        this.certification = certification;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Gem \n\tname: " + name + "\n\torigin: " + origin +
                "\n\tweight: " + caratWeight + "\n\tcertification: " +
                certification + "\n\tvalue: " + value + "\n" +
                visualParameters;
    }
}

class VisualParameters {
    private String colour;
    private boolean transparency;
    private String clarity;
    private String cut;
    private boolean colourChange;
    private boolean catsEye;

    @Override
    public String toString() {
        return "\n\tcolour: " + colour + "\n\ttransparency: " + transparency +
                "\n\tclarity: " + clarity + "\n\tcut: " + cut +
                "\n\tcolour change: " + colourChange + "\n\tcat's eye: " +
                catsEye + "\n";
    }

    public VisualParameters() { }

    protected VisualParameters(String colour, boolean transparency, String clarity, String cut,
                             boolean colourChange, boolean catsEye) {
        this.colour = colour;
        this.transparency = transparency;
        this.clarity = clarity;
        this.cut = cut;
        this.colourChange = colourChange;
        this.catsEye = catsEye;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public boolean isTransparency() {
        return transparency;
    }

    public void setTransparency(boolean transparency) {
        this.transparency = transparency;
    }

    public String getClarity() {
        return clarity;
    }

    public void setClarity(String clarity) {
        this.clarity = clarity;
    }

    public String getCut() {
        return cut;
    }

    public void setCut(String cut) {
        this.cut = cut;
    }

    public boolean isColourChange() {
        return colourChange;
    }

    public void setColourChange(boolean colourChange) {
        this.colourChange = colourChange;
    }

    public boolean isCatsEye() {
        return catsEye;
    }

    public void setCatsEye(boolean catsEye) {
        this.catsEye = catsEye;
    }
}
