package me.krawtschenko.yulia;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.ValidationMessage;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Set;

public class GemParserJackson implements GemParser {
    private final ObjectMapper mapper;

    public GemParserJackson() {
        mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.FAIL_ON_READING_DUP_TREE_KEY);
        mapper.enable(DeserializationFeature.FAIL_ON_TRAILING_TOKENS);
        mapper.enable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
        mapper.enable(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY);
    }

    @Override
    public boolean validate(final Path json) throws IOException {
        boolean isValid = true;

        try {
            mapper.readTree(json.toFile());
        } catch (JsonProcessingException e) {
            isValid = false;
        }

        return isValid;
    }

    public boolean validateAgainstSchema(final Path jsonPath,
                                         final Path schemaPath)
            throws IOException {
        JsonSchemaFactory schemaFactory = JsonSchemaFactory.getInstance();
        JsonSchema schema = schemaFactory.getSchema(schemaPath.toUri());

        JsonNode node = mapper.readTree(jsonPath.toFile());

        Set<ValidationMessage> errors = schema.validate(node);

        return errors.size() == 0;
    }

    @Override
    public Gem[] parseGems(final Path path) throws IOException {
        return mapper.readValue(path.toFile(), Gem[].class);
    }
}
