package me.krawtschenko.yulia;

import java.io.IOException;
import java.nio.file.Path;

public interface GemParser {
    boolean validate(final Path path) throws IOException;
    Gem[] parseGems(final Path path) throws IOException;
}
