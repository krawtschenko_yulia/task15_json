package me.krawtschenko.yulia;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class Application {
    public static void main(String[] args) throws IOException {
        Logger logger = LoggerFactory.getLogger(Application.class);

        Path gemsPath = Paths.get("src/main/resources/gems.json");
        Path gemsSchema = Paths.get("src/main/resources/gemsSchema.json");

        GemParserJackson jacksonParser = new GemParserJackson();
        if (jacksonParser.validate(gemsPath) &&
                jacksonParser.validateAgainstSchema(gemsPath, gemsSchema)) {

            Gem[] gems = jacksonParser.parseGems(gemsPath);
            for (Gem gem : gems) {
                System.out.println(gem);
            }

            GemParserGson gsonParser = new GemParserGson();
            gems = gsonParser.parseGems(gemsPath);
            System.out.println(Arrays.toString(gems));
        }
    }
}
