package me.krawtschenko.yulia;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

public class GemParserGson implements GemParser {
    private Gson mapper;

    public GemParserGson() {
        mapper = new Gson();
    }

    @Override
    public boolean validate(final Path json) throws IOException {
        boolean isValid = true;

        try {
            mapper.fromJson(new FileReader(json.toFile()), Object.class);
        } catch (JsonSyntaxException e) {
            isValid = false;
        }

        return isValid;
    }

    @Override
    public Gem[] parseGems(final Path path) throws IOException {
        return mapper.fromJson(new FileReader(path.toFile()), Gem[].class);
    }
}
