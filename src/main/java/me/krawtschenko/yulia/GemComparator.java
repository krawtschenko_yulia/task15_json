package me.krawtschenko.yulia;

import java.util.Comparator;

public class GemComparator implements Comparator<Gem> {
    @Override
    public int compare(Gem one, Gem two) {
        if (one.getName().equals(two.getName())) {
            return (int) Math
                    .floor(one.getCaratWeight() - two.getCaratWeight());
        }
        return one.getName().compareToIgnoreCase(two.getName());
    }
}